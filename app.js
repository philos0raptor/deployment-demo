var express = require("express");
var app = express();

app.set("view engine", "ejs");

app.get("/", function(req, res){
    res.render("home");
});


app.get("/about", function(req, res){
    res.render("about");
});

app.get("/faq", function(req, res){
    res.render("faq");
});

app.listen(process.env.PORT, process.env.IP);
console.log("Application running");
